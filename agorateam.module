<?php

/**
 * @file
 * Defines common functionality for agorateam module.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountInterface;

/**
 * Implements hook_theme().
 */
function agorateam_theme() {
  return [
    'employee' => [
      'render element' => 'elements',
    ],
  ];
}

/**
 * Prepares variables for employee templates.
 *
 * Default template: employee.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing rendered fields.
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_employee(array &$variables) {
  /** @var Drupal\agorateam\Entity\EmployeeInterface $employee */
  $employee = $variables['elements']['#employee'];

  $variables['employee_entity'] = $employee;
  $variables['label'] = $employee->label();
  $variables['content'] = [];
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function agorateam_theme_suggestions_employee(array $variables) {
  $suggestions = [];
  /** @var Drupal\agorateam\Entity\EmployeeInterface $employee */
  $employee = $variables['elements']['#employee'];
  $sanitized_view_mode = strtr($variables['elements']['#view_mode'], '.', '_');

  $suggestions[] = 'employee__' . $sanitized_view_mode;
  $suggestions[] = 'employee__' . $employee->bundle();
  $suggestions[] = 'employee__' . $employee->bundle() . '__' . $sanitized_view_mode;
  $suggestions[] = 'employee__' . $employee->id();
  $suggestions[] = 'employee__' . $employee->id() . '__' . $sanitized_view_mode;

  return $suggestions;
}

/**
 * Implements hook_ENTITY_TYPE_create_access() for 'employee' entity type.
 */
function agorateam_employee_create_access(AccountInterface $account, array $context, $entity_bundle) {
  return AccessResult::allowedIfHasPermission($account, 'create employee');
}

/**
 * Implements hook_ENTITY_TYPE_access() for 'employee' entity type.
 */
function agorateam_employee_access(EntityInterface $entity, $operation, AccountInterface $account) {
  switch ($operation) {
    case 'view':
      $result = AccessResult::allowed();
      break;

    case 'update':
      // @todo 'edit own employee'
      $result = AccessResult::allowedIfHasPermission($account, 'edit any employee');
      break;

    case 'delete':
      $result = AccessResult::allowedIfHasPermission($account, 'administer employee');
      break;

    default:
      $result = AccessResult::neutral();
  }

  return $result;
}
