<?php

namespace Drupal\agorateam_teams\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for teams.
 */
interface TeamInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the team name.
   *
   * @return string
   *   The team name.
   */
  public function getName(): string;

  /**
   * Sets the team name.
   *
   * @param string $name
   *   The team name.
   *
   * @return $this
   */
  public function setName(string $name): TeamInterface;

  /**
   * Get the employees.
   *
   * @return \Drupal\agorateam\Entity\EmployeeInterface[]
   *   The employees.
   */
  public function getEmployees(): array;

  /**
   * Set the employees.
   *
   * @param \Drupal\agorateam\Entity\EmployeeInterface[] $employees
   *   The employees.
   *
   * @return $this
   */
  public function setEmployees(array $employees): TeamInterface;

  /**
   * Gets the creation timestamp.
   *
   * @return int
   *   The creation timestamp.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the creation timestamp.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime(int $timestamp): TeamInterface;

}
