<?php

namespace Drupal\agorateam_teams\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the team type entity class.
 *
 * @ConfigEntityType(
 *   id = "team_type",
 *   label = @Translation("Team type"),
 *   label_singular = @Translation("Team type"),
 *   label_plural = @Translation("Team types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count team type",
 *     plural = "@count team types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\agorateam_teams\Form\TeamTypeForm",
 *       "edit" = "Drupal\agorateam_teams\Form\TeamTypeForm",
 *       "delete" = "Drupal\agorateam_teams\Form\TeamTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\agorateam_teams\TeamTypeListBuilder",
 *   },
 *   config_prefix = "team_type",
 *   admin_permission = "administer team types",
 *   bundle_of = "team",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/team-types/add",
 *     "edit-form" = "/admin/structure/team-types/{team_type}/edit",
 *     "delete-form" = "/admin/structure/team-types/{team_type}/delete",
 *     "collection" = "/admin/structure/team-types"
 *   }
 * )
 */
class TeamType extends ConfigEntityBundleBase implements TeamTypeInterface {

  /**
   * The team type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The team type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The team type description.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
