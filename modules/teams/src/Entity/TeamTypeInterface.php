<?php

namespace Drupal\agorateam_teams\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for team types.
 */
interface TeamTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
