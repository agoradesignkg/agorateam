<?php

namespace Drupal\agorateam_teams\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subcriber extending permissions for team collection route.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // For taxonomy term view route, set our custom controller instead.
    if ($route = $collection->get('entity.team.collection')) {
      $perm = sprintf('%s+%s', $route->getRequirement('_permission'), 'access employee overview');
      $route->setRequirement('_permission', $perm);
    }
  }

}
