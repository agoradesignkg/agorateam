<?php

namespace Drupal\agorateam_teams\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the team add/edit form.
 */
class TeamForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\agorateam_teams\Entity\TeamInterface $team */
    $team = $this->getEntity();
    $team->save();
    $this->messenger()->addStatus($this->t('The team %label has been successfully saved.', ['%label' => $team->label()]));
    $form_state->setRedirect('entity.team.collection', ['team' => $team->id()]);
  }

}
