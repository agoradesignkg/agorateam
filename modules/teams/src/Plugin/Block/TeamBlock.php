<?php

namespace Drupal\agorateam_teams\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the team block.
 *
 * @Block(
 *   id = "team",
 *   admin_label = @Translation("Team"),
 *   category = "HUMER"
 * )
 */
class TeamBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * The team storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $teamStorage;

  /**
   * The team view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $teamViewBuilder;

  /**
   * Constructs a new UserNavBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, EntityDisplayRepositoryInterface $entity_display_repository, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityDisplayRepository = $entity_display_repository;
    $this->teamStorage = $entity_type_manager->getStorage('team');
    $this->teamViewBuilder = $entity_type_manager->getViewBuilder('team');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_display.repository'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'team' => NULL,
      'view_mode' => 'full',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $available_teams = $this->teamStorage->loadByProperties(['status' => 1]);
    $options = [];
    foreach ($available_teams as $team) {
      $options[$team->id()] = $team->label();
    }

    $form['team'] = [
      '#type' => 'select',
      '#title' => $this->t('Team'),
      '#default_value' => $this->configuration['team'],
      '#options' => $options,
      '#required' => TRUE,
    ];

    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#description' => $this->t('The view mode that will be used for rendering the team in the block.'),
      '#default_value' => $this->configuration['view_mode'],
      '#options' => $this->getAvailableViewModes(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['team'] = $form_state->getValue('team');
    $this->configuration['view_mode'] = $form_state->getValue('view_mode');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    /** @var \Drupal\agorateam_teams\Entity\TeamInterface $team */
    $team = $this->teamStorage->load($this->configuration['team']);
    if ($team && $team->isPublished()) {
      $build = $this->teamViewBuilder->view($team, $this->configuration['view_mode']);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(['employee_list', 'team_list'], parent::getCacheTags());
  }

  /**
   * Gets available view modes of team entities for block form configuration.
   *
   * @return string[]
   *   The available view modes.
   */
  protected function getAvailableViewModes(): array {
    $options = [
      // Always add the 'default' view mode.
      'default' => 'Default',
    ];
    $form_modes = $this->entityDisplayRepository->getViewModes('team');
    foreach ($form_modes as $id => $info) {
      $options[$id] = $info['label'];
    }
    return $options;
  }

}
