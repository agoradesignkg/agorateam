<?php

namespace Drupal\agorateam\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\file\Entity\File;
use Drupal\taxonomy\TermInterface;
use Drupal\user\UserInterface;

/**
 * Defines the employee entity class.
 *
 * @ContentEntityType(
 *   id = "employee",
 *   label = @Translation("Employee"),
 *   label_singular = @Translation("Employee"),
 *   label_plural = @Translation("Employees"),
 *   label_count = @PluralTranslation(
 *     singular = "@count employee",
 *     plural = "@count employees",
 *   ),
 *   bundle_label = @Translation("Employee type"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Entity\Sql\SqlContentEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\agorateam\EmployeeListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\agorateam\Form\EmployeeForm",
 *       "add" = "Drupal\agorateam\Form\EmployeeForm",
 *       "edit" = "Drupal\agorateam\Form\EmployeeForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer employees",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   base_table = "employee",
 *   data_table = "employee_field_data",
 *   entity_keys = {
 *     "id" = "employee_id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *     "published" = "status",
 *   },
 *   links = {
 *     "add-page" = "/employee/add",
 *     "add-form" = "/employee/add/{employee_type}",
 *     "edit-form" = "/employee/{employee}/edit",
 *     "delete-form" = "/employee/{employee}/delete",
 *     "collection" = "/admin/content/employees"
 *   },
 *   bundle_entity_type = "employee_type",
 *   field_ui_base_route = "entity.employee_type.edit_form",
 * )
 */
class Employee extends ContentEntityBase implements EmployeeInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return (int) $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDepartment() {
    return $this->get('department')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setDepartment(TermInterface $department) {
    $this->set('department', $department->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDepartmentId() {
    return $this->get('department')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setDepartmentId($department_id) {
    $this->set('department', $department_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPosition() {
    return $this->get('position')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setPosition(TermInterface $position) {
    $this->set('position', $position->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPositionId() {
    return $this->get('position')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPositionId($position_id) {
    $this->set('position', $position_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPhoto() {
    return $this->get('photo')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setPhoto(File $photo) {
    $this->set('photo', $photo->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPhotoId() {
    return $this->get('photo')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPhotoId($file_id) {
    $this->set('photo', $file_id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPhoneNumber() {
    return $this->get('phone')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPhoneNumber($phone_number) {
    $this->set('phone', $phone_number);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEmail() {
    return $this->get('mail')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEmail($mail) {
    $this->set('mail', $mail);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->get('field_weight')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->set('field_weight', $weight);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    return $this->set('uid', $uid);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The author.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDefaultValueCallback('Drupal\agorateam\Entity\Employee::getCurrentUserId');

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the employee.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('default_value', '')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['photo'] = BaseFieldDefinition::create('image')
      ->setLabel(t('Photo'))
      ->setDescription(t('Photo of the employee.'))
      ->setRequired(FALSE)
      ->setTranslatable(TRUE)
      ->setSetting('file_extensions', 'png gif jpg jpeg')
      ->setSetting('alt_field', TRUE)
      ->setSetting('alt_field_required', FALSE)
      ->setSetting('title_field', TRUE)
      ->setSetting('title_field_required', FALSE)
      ->setSetting('file_directory', 'team')
      ->setDisplayOptions('form', [
        'type' => 'image_image',
        'weight' => 1,
        'settings' => [
          'progress_indicator' => 'throbber',
          'preview_image_style' => 'thumbnail',
        ],
      ])
      ->setDisplayOptions('view', [
        'type' => 'image',
        'weight' => 1,
        'label' => 'hidden',
        'settings' => [
          'image_link' => '',
          'image_style' => 'team',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['department'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Department'))
      ->setDescription(t('The department of the employee.'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => ['departments' => 'departments'],
        'sort' => ['field' => '_none'],
        'auto_create' => FALSE,
        'auto_create_bundle' => '',
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['position'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Position'))
      ->setDescription(t('The job position of the employee.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default:taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => ['job_positions' => 'job_positions'],
        'sort' => ['field' => '_none'],
        'auto_create' => TRUE,
        'auto_create_bundle' => 'job_positions',
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['mail'] = BaseFieldDefinition::create('email')
      ->setLabel(t('Contact email'))
      ->setDescription(t('The email address of the employee.'))
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'email_default',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['phone'] = BaseFieldDefinition::create('telephone')
      ->setLabel(t('Phone number'))
      ->setDescription(t('The phone number of the employee.'))
      ->setDefaultValue('')
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'telephone_default',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the employee was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the employee was last edited.'))
      ->setTranslatable(TRUE);

    $fields['status']
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 99,
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

}
