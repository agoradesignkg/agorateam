<?php

namespace Drupal\agorateam\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\file\Entity\File;
use Drupal\taxonomy\TermInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for employees.
 */
interface EmployeeInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, EntityPublishedInterface {

  /**
   * Gets the employee's name.
   *
   * @return string
   *   The employee's name.
   */
  public function getName();

  /**
   * Sets the employee's name.
   *
   * @param string $name
   *   The employee's name.
   *
   * @return $this
   */
  public function setName($name);

  /**
   * Gets the creation timestamp.
   *
   * @return int
   *   The creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the creation timestamp.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the department.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   The department term, or null.
   */
  public function getDepartment();

  /**
   * Sets the department.
   *
   * @param \Drupal\taxonomy\TermInterface $department
   *   The department term.
   *
   * @return $this
   */
  public function setDepartment(TermInterface $department);

  /**
   * Gets the department ID.
   *
   * @return int
   *   The department ID.
   */
  public function getDepartmentId();

  /**
   * Sets the department ID.
   *
   * @param int $department_id
   *   The department ID.
   *
   * @return $this
   */
  public function setDepartmentId($department_id);

  /**
   * Gets the job position.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   The job position term, or null.
   */
  public function getPosition();

  /**
   * Sets the job position.
   *
   * @param \Drupal\taxonomy\TermInterface $position
   *   The job position term.
   *
   * @return $this
   */
  public function setPosition(TermInterface $position);

  /**
   * Gets the job position ID.
   *
   * @return int
   *   The job position ID.
   */
  public function getPositionId();

  /**
   * Sets the job position ID.
   *
   * @param int $position_id
   *   The job position ID.
   *
   * @return $this
   */
  public function setPositionId($position_id);

  /**
   * Gets the photo file.
   *
   * @return \Drupal\file\Entity\File|null
   *   The photo file entity, or null.
   */
  public function getPhoto();

  /**
   * Sets the photo file.
   *
   * @param \Drupal\file\Entity\File $photo
   *   The photo file entity.
   *
   * @return $this
   */
  public function setPhoto(File $photo);

  /**
   * Gets the photo file ID.
   *
   * @return int
   *   The photo file ID.
   */
  public function getPhotoId();

  /**
   * Sets the photo file ID.
   *
   * @param int $file_id
   *   The photo file ID.
   *
   * @return $this
   */
  public function setPhotoId($file_id);

  /**
   * Gets the employee's phone number.
   *
   * @return string
   *   The employee's phone number.
   */
  public function getPhoneNumber();

  /**
   * Sets the employee's phone number.
   *
   * @param string $phone_number
   *   The employee's phone number.
   *
   * @return $this
   */
  public function setPhoneNumber($phone_number);

  /**
   * Gets the employee's email.
   *
   * @return string
   *   The employee's email.
   */
  public function getEmail();

  /**
   * Sets the employee's email.
   *
   * @param string $mail
   *   The employee's email.
   *
   * @return $this
   */
  public function setEmail($mail);

  /**
   * Gets the employee's sort weight.
   *
   * @return int
   *   The employee's sort weight.
   */
  public function getWeight();

  /**
   * Sets the employee's sort weight.
   *
   * @param int $weight
   *   The employee's sort weight.
   *
   * @return $this
   */
  public function setWeight($weight);

}
