<?php

namespace Drupal\agorateam\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the employee type entity class.
 *
 * @ConfigEntityType(
 *   id = "employee_type",
 *   label = @Translation("Employee type"),
 *   label_singular = @Translation("Employee type"),
 *   label_plural = @Translation("Employee types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count employee type",
 *     plural = "@count employee types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\agorateam\Form\EmployeeTypeForm",
 *       "edit" = "Drupal\agorateam\Form\EmployeeTypeForm",
 *       "delete" = "Drupal\agorateam\Form\EmployeeTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\agorateam\EmployeeTypeListBuilder",
 *   },
 *   config_prefix = "employee_type",
 *   admin_permission = "administer employee types",
 *   bundle_of = "employee",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/employee-types/add",
 *     "edit-form" = "/admin/structure/employee-types/{employee_type}/edit",
 *     "delete-form" = "/admin/structure/employee-types/{employee_type}/delete",
 *     "collection" = "/admin/structure/employee-types"
 *   }
 * )
 */
class EmployeeType extends ConfigEntityBundleBase implements EmployeeTypeInterface {

  /**
   * The employee type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The employee type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The employee type description.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
