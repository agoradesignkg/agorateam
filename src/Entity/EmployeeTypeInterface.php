<?php

namespace Drupal\agorateam\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for employee types.
 */
interface EmployeeTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
