<?php

namespace Drupal\agorateam\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the employee add/edit form.
 */
class EmployeeForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\agorateam\Entity\EmployeeInterface $employee */
    $employee = $this->getEntity();
    $employee->save();
    $this->messenger()->addStatus($this->t('The employee %label has been successfully saved.', ['%label' => $employee->label()]));
    $form_state->setRedirect('entity.employee.collection', ['employee' => $employee->id()]);
  }

}
